import 'package:new_year_resolution_tracker/components/calendar_list.dart';
import 'package:toast/toast.dart';
import 'models/resolution.dart';
import 'package:flutter/material.dart';

class ResolutionDetail extends StatelessWidget {
  final Resolution resolution;
  const ResolutionDetail(this.resolution);

  Widget _renderDesc(Resolution resolution) {
    return Flexible(
      child: Text(
        resolution.desc,
        style: const TextStyle(fontSize: 18),
      ),
    );
  }

  Widget _resolutionIcon(BuildContext context, Icon icon) {
    double height = MediaQuery.of(context).size.height;

    return Container(
      child: icon,
      height: height * 0.2,
      alignment: Alignment.center,
    );
  }

  Row _renderBody(BuildContext context, Resolution resolution) {
    double width = MediaQuery.of(context).size.width;

    return Row(
      children: [
        SizedBox(
          child: _resolutionIcon(context, Icon(resolution.icon, size: 50)),
          width: width * 0.3,
        ),
        _renderDesc(resolution)
      ],
    );
  }

  ElevatedButton doneButton(Resolution r) {
    return ElevatedButton.icon(
        onPressed: () {
          if (r.complete()) {
            Toast.show("Congrats!",
                duration: Toast.lengthLong, gravity: Toast.bottom);
          } else {
            Toast.show("You have already completed your task.",
                duration: Toast.lengthLong, gravity: Toast.bottom);
          }
        },
        icon: const Icon(Icons.check),
        label: const Text("Done"));
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    ToastContext().init(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(resolution.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _renderBody(context, resolution),
          doneButton(resolution),
          Expanded(
            child: CalendarList(resolution.resolutionInterval),
          ),
        ],
      ),
    );
  }
}
