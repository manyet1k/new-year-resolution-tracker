import 'package:flutter/material.dart';
import 'package:new_year_resolution_tracker/resolution_detail.dart';
import '../models/resolution.dart';

class listResolutions extends StatelessWidget {
  var resolutions = <Resolution>[];
  bool usecase; //0 <-> home, 1 <-> edit
  listResolutions(this.usecase);

  var tmp = Resolution(
      "fdsf",
      "dfdas fj alsıdfjdsafdasfs adfds fdsfdsaf dsafdsfdsafds fads",
      ResolutionInterval.Daily,
      Icons.access_alarm);

  Widget _itemThumbnail(Resolution thumbnailLocation) {
    return Container(
      constraints: const BoxConstraints.tightFor(width: 100),
      child: Icon(thumbnailLocation.icon),
    );
  }

  Widget _itemTitle(Resolution titleLocation) {
    return Text(titleLocation.title);
  }

  Widget _resolutionTile(BuildContext context, int index) {
    final res = resolutions[index];
    return ListTile(
      contentPadding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
      leading: _itemThumbnail(res),
      title: _itemTitle(res),
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => ResolutionDetail(res)));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    resolutions.add(tmp);
    resolutions.add(Resolution("weekly", "weeekly task for debugging purposes",
        ResolutionInterval.Weekly, Icons.zoom_out));
    resolutions.add(Resolution("monthly", "monthly task for debugging purposes",
        ResolutionInterval.Monthly, Icons.face));
    resolutions.add(Resolution(
        "no interval",
        "task without interval for debugging purposes",
        ResolutionInterval.None,
        Icons.javascript));
    return ListView.builder(
        itemCount: resolutions.length, itemBuilder: _resolutionTile);
  }
}
