import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/resolution.dart';

class CalendarList extends StatelessWidget {
  ResolutionInterval ri = ResolutionInterval.None;
  CalendarList(this.ri);

  @override
  Widget build(BuildContext context) {
    final currentDate = DateTime.now();
    final diff = currentDate.difference(DateTime(currentDate.year, 1, 1, 0, 0));

    int ic() {
      if (ri == ResolutionInterval.Daily) {
        return (currentDate.year % 4 != 0 || currentDate.year % 100 == 0)
            ? 365
            : 366;
      }
      if (ri == ResolutionInterval.Weekly) {
        return 52;
      }
      if (ri == ResolutionInterval.Monthly) {
        return 12;
      } else {
        return 1;
      }
    }

    Text ib(int index) {
      if (ri == ResolutionInterval.Daily) {
        return Text(
            "${DateFormat.yMMMMd().format(currentDate.subtract(diff).add(Duration(days: index)))} ");
      }
      if (ri == ResolutionInterval.Weekly) {
        return Text("Week " + (index + 1).toString());
      }
      if (ri == ResolutionInterval.Monthly) {
        return Text(
            "${DateFormat.MMMM().format(currentDate.subtract(diff).add(Duration(days: index * 30 + 10)))} ");
      } else {
        return Text("Status");
      }
    }

    return ListView.builder(
      shrinkWrap: true,
      itemCount: ic(),
      itemBuilder: (_, index) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
          child: ib(index),
        );
      },
    );
  }
}
