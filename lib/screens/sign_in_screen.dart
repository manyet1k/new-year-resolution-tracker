import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:new_year_resolution_tracker/screens/homescreen.dart';
import 'package:toast/toast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'sign_up_screen.dart';

class SignInScreen extends StatelessWidget {
  SignInScreen({Key? key}) : super(key: key);

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
        appBar: AppBar(
          title: const Text("Sign In"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              EmailTextField(width),
              const SizedBox(
                height: 10,
              ),
              PasswordTextField(width),
              ElevatedButton(
                child: const Text("Sign In"),
                onPressed: () {
                  signIn(context);
                },
              ),
              const SizedBox(height: 10),
              ElevatedButton(
                child: const Text("Don't have an account? Sign Up"),
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => SignUpScreen()),
                  );
                },
              ),
            ],
          ),
        ));
  }

  signIn(BuildContext context) async {
    bool isEmailAllowed = checkEmail();
    if (!isEmailAllowed) {
      Toast.show("Invalid email address",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return;
    }

    // Email is allowed and user will sign in
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: emailController.text, password: passwordController.text);
    } catch (e) {
      Toast.show("Can't sign in, please try again",
          duration: Toast.lengthLong, gravity: Toast.bottom);
    }

    if (FirebaseAuth.instance.currentUser != null) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => home_screen()),
      );
    }
  }

  bool checkEmail() {
    final emailTextRegex =
        RegExp(r"^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$");
    return emailTextRegex.hasMatch(emailController.text);
  }

  SizedBox EmailTextField(double width) {
    return SizedBox(
      width: width * 0.7,
      child: TextField(
        controller: emailController,
        decoration: const InputDecoration(
          prefixIcon: Icon(Icons.person),
          border: OutlineInputBorder(),
          label: Text("Email"),
        ),
      ),
    );
  }

  SizedBox PasswordTextField(double width) {
    return SizedBox(
      width: width * 0.7,
      child: TextField(
        controller: passwordController,
        obscureText: true,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          prefixIcon: Icon(Icons.lock),
          label: Text("Password"),
        ),
      ),
    );
  }
}
