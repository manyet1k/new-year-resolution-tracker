import 'package:flutter/material.dart';
import 'package:new_year_resolution_tracker/components/custom_drawer.dart';
import 'package:new_year_resolution_tracker/screens/add_resolutions_screen.dart';

class EditResolutionsScreen extends StatelessWidget {
  const EditResolutionsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CustomDrawer(),
      appBar: AppBar(
        title: const Text("Edit Resolutions"),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).colorScheme.primary,
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AddResolutionsScreen()),
          );
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
