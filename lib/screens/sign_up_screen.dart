import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:new_year_resolution_tracker/screens/homescreen.dart';
import 'package:toast/toast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'sign_in_screen.dart';

class SignUpScreen extends StatelessWidget {
  SignUpScreen({Key? key}) : super(key: key);

  TextEditingController emailController = TextEditingController();
  TextEditingController firstPasswordController = TextEditingController();
  TextEditingController secondPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        title: const Text("Sign Up"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 24.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              EmailTextField(width),
              const SizedBox(height: 10),
              FirstPasswordTextField(width),
              const SizedBox(height: 10),
              SecondPasswordTextField(width),
              ElevatedButton(
                child: const Text("Sign Up"),
                onPressed: () {
                  signUp(context);
                },
              ),
              const SizedBox(height: 10),
              ElevatedButton(
                child: const Text("Already have an account? Sign In"),
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => SignInScreen()),
                  );
                },
              ),
              const SizedBox(height: 25),
              PasswordRequirements(height),
            ],
          ),
        ),
      ),
    );
  }

  Container PasswordRequirements(double height) {
    return Container(
      margin: const EdgeInsets.all(32),
      padding: const EdgeInsets.only(top: 16),
      height: height * 0.3,
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xFFE61A1A),
          width: 4,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      child: GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 2,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: const [
          ListTile(
            leading: Icon(Icons.done),
            minLeadingWidth: 10,
            title: Text(
              "At least 1 lowercase alphabetical character",
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
            ),
          ),
          ListTile(
            leading: Icon(Icons.done),
            minLeadingWidth: 10,
            title: Text(
              "At least 1 uppercase alphabetical character",
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
            ),
          ),
          ListTile(
            leading: Icon(Icons.done),
            minLeadingWidth: 10,
            title: Text(
              "At least 1 numeric character",
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
            ),
          ),
          ListTile(
            leading: Icon(Icons.done),
            minLeadingWidth: 10,
            title: Text(
              "At least 1 special character",
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
            ),
          ),
          ListTile(
            leading: Icon(Icons.done),
            minLeadingWidth: 10,
            title: Text(
              "Eight characters or longer",
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
            ),
          ),
        ],
      ),
    );
  }

  signUp(BuildContext context) async {
    bool isEmailAllowed = checkEmail();
    bool arePasswordsSame = checkIfPasswordsSame();
    bool isPasswordStrong = checkIfPasswordStrong();

    var errorMessage = "";
    if (!isEmailAllowed) errorMessage += "Invalid email address.";

    if (!arePasswordsSame) {
      if (!isEmailAllowed) errorMessage += " ";
      errorMessage += "Passwords don't match.";
    } else if (!isPasswordStrong) {
      if (!isEmailAllowed) errorMessage += " ";
      errorMessage += "The password provided is too weak.";
    }

    if (errorMessage != "") {
      Toast.show(errorMessage,
          duration: Toast.lengthLong, gravity: Toast.bottom);
      return;
    }

    // Email is allowed and user will sign in
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: emailController.text, password: firstPasswordController.text);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'email-already-in-use') {
        Toast.show("The account already exists for that email.",
            duration: Toast.lengthLong, gravity: Toast.bottom);
      }
    } catch (e) {
      Toast.show("Can't sign up, please try again",
          duration: Toast.lengthLong, gravity: Toast.bottom);
    }

    if (FirebaseAuth.instance.currentUser != null) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => home_screen()),
      );
    }
  }

  bool checkEmail() {
    final emailTextRegex =
        RegExp(r"^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$");

    return emailTextRegex.hasMatch(emailController.text);
  }

  bool checkIfPasswordsSame() {
    var firstPassword = firstPasswordController.text;
    var secondPassword = secondPasswordController.text;

    if (firstPassword != secondPassword) return false;
    return true;
  }

  bool checkIfPasswordStrong() {
    final passwordRegex = RegExp(
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    return passwordRegex.hasMatch(firstPasswordController.text);
  }

  SizedBox EmailTextField(double width) {
    return SizedBox(
      width: width * 0.7,
      child: TextField(
        controller: emailController,
        decoration: const InputDecoration(
          prefixIcon: Icon(Icons.person),
          border: OutlineInputBorder(),
          label: const Text("Email"),
        ),
      ),
    );
  }

  SizedBox FirstPasswordTextField(double width) {
    return SizedBox(
      width: width * 0.7,
      child: TextField(
        controller: firstPasswordController,
        obscureText: true,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          prefixIcon: Icon(Icons.lock),
          label: Text("Password"),
        ),
      ),
    );
  }

  SizedBox SecondPasswordTextField(double width) {
    return SizedBox(
      width: width * 0.7,
      child: TextField(
        controller: secondPasswordController,
        obscureText: true,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          prefixIcon: Icon(Icons.lock),
          label: Text("Confirm Password"),
        ),
      ),
    );
  }
}
