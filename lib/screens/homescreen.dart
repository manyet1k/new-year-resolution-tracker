import 'package:flutter/material.dart';
import 'package:new_year_resolution_tracker/components/custom_drawer.dart';
import '../models/resolution.dart';
import 'package:new_year_resolution_tracker/resolution_detail.dart';
import '../components/list_resolutions.dart';

class home_screen extends StatelessWidget {
  home_screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
        ),
        drawer: CustomDrawer(),
        body: listResolutions(false));
  }
}
