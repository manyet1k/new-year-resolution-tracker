import 'dart:core';
import 'package:intl/intl.dart';
import 'package:week_of_year/week_of_year.dart';
import 'package:flutter/material.dart';

enum ResolutionInterval { Daily, Weekly, Monthly, None }

class Resolution {
  String title = "";
  String desc = "";
  var resolutionInterval = ResolutionInterval.Daily;
  IconData icon = Icons.abc;
  Set<String> completed = {};

  Resolution(this.title, this.desc, this.resolutionInterval, this.icon);

  bool complete() {
    if (title == "") return false;

    if (resolutionInterval == ResolutionInterval.Daily) {
      return completed.add(DateFormat.yMMMEd().format(DateTime.now()));
    }
    if (resolutionInterval == ResolutionInterval.Weekly) {
      return completed.add(DateTime.now().weekOfYear.toString());
    }
    if (resolutionInterval == ResolutionInterval.Monthly) {
      return completed.add(DateFormat("MM").format(DateTime.now()));
    } else {
      return completed.add("Done");
    }
  }
}
