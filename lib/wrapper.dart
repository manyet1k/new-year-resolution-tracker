import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:new_year_resolution_tracker/screens/homescreen.dart';
import 'package:new_year_resolution_tracker/screens/sign_up_screen.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var user = FirebaseAuth.instance.currentUser;

    if (user != null)
      return home_screen();
    else
      return SignUpScreen();
  }
}
