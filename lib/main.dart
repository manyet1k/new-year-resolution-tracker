import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:new_year_resolution_tracker/wrapper.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dashboard',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: const Color(0xFFE61A1A),
          secondary: const Color(0xFFFFFFFF),
        ),
      ),
      home: Wrapper(),
    );
  }
}
